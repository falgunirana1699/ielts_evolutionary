'use strict';

/**
 * Module dependencies.
 */

import assrt from 'assert';
import should from 'should';
import mongoose from 'mongoose';
import {expect} from 'chai';
import Server from "../../../base-server/index"
import baseApp from  '../../../base-server/server/server';
import { config } from '../config/positions.config';
import supertest from 'supertest';
import { createMockData } from './create.mock';
import { listMockData } from './list.mock';
import chai from 'chai';
import chaiHttp from 'chai-http';
import { PositionSchema } from "../server/models/evolution.model";
import { routes } from "../server/routes/evolution.routes";

chai.use(chaiHttp);


/**
 * Unit tests
 */


describe('Controller Test', function() {
  let server, db, app;
  let agent;
  this.timeout(150000);
  before( async () => {
    
    server = new Server(config);   
    [db, app] = await server.start(routes); 
    //agent =  supertest.agent(app);
  });
  
  describe("Create Test", ()=>{

    let result;
    before((done)=>{
      chai.request(app)
      .evolution('/api/v1/positions/')
      .send(createMockData.body)
      .end((err, res)=>{
        result = res;
        done();
      })
    });

    it('success function sends something',() => {
      expect(result).not.to.be.empty;
    });
    
    
    it('success function sends an object', function(){
      expect(result).to.be.instanceof(Object);
    });

    it('should send  200 Status Code ', function() {
      expect(result).to.have.property('statusCode', 200);
    });

    it('success object should contain body', function(){
        expect(result).to.have.property('body');
    });

    it('success object should not contain empty body', function(){
      expect(result.body).not.to.be.empty;
    });

    it('success body should contain body ', function() {
      expect(result.body).to.have.property('body');
    });

    it('success body will contain non empty body', function(){
        expect(result.body.body).not.to.be.empty;
    });

    it('result should have mandatory fields', function(){
           
      expect(result.body.body).to.contain.all.keys(createMockData.mandatoryFields);
     
    });          

  })


  describe("List Test", ()=>{

    let result;
    before((done)=>{
      chai.request(app)
      .get('/api/v1/positions/')
      .send()
      .end((err, res)=>{
        result = res;
        done();
      })
    });

    it('success function sends something', function(){
      expect(result).not.to.be.empty;
    });        

    it('success function sends an object', function(){
        expect(result).to.be.instanceof(Object);
    });        


    it('should send  200 Status Code ', function() {
      expect(result).to.have.property('statusCode', 200);
    });

    it('success object should contain body', function(){
        expect(result).to.have.property('body');
    });

    it('success object should not contain empty body', function(){
        expect(result.body).not.to.be.empty;
    });

    it('success  body will contain body', function(){
        expect(result.body.body).not.to.be.empty;
    });

    it('suceess bodys body should have be an Array', function(){
        expect(result.body.body.data).to.be.instanceOf(Array)
    });       

    it('Array Item should Contain all mandatory criteria', function(){
        expect(result.body.body.data[0]).to.contain.all.keys(listMockData.mandatoryFields);
    });    

    
  });


  


});
