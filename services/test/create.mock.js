const createMockData = 
{

    "body": {
        "required_min_exp": 3,
        "description": "Developer",
        "name": "Developer",
        "account_name":"Test Account Name",
        "total_position": 2,
        "location":"Hydrabad",
        "technology": "Php",
        "project_name": "ATS",
        "start_date":"2018-09-09T09:18:42.551Z",
        "owner_name": "HR",
        "candidate_level": "L3",
        "comments": "Any Comments ?"
        
    },   
    "mandatoryFields":[
        "account_name",
        "description",
        "required_min_exp",
        "total_position",
        "location",
        "technology",
        "owner_name",
        "candidate_level",
        "createdAt",
        "lastUpdatedAt",
        "_id"
    ]

    
}
export {createMockData} 