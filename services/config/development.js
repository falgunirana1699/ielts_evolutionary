
const config = {
    db: {
        uri: process.env.MONGOHQ_URL || process.env.MONGODB_URI || 'mongodb://' + (process.env.DB_1_PORT_27017_TCP_ADDR || 'localhost:27017') + '/db_test',
        options: {
            // evolution: 'admin',
            // pass: 'admin'
        },
        // Enable mongoose debug mode
        debug: process.env.MONGODB_DEBUG || false
      },
    port:3020
}


export {config} 
