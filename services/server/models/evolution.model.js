'use strict';

/**
 * Module dependencies
 */
import mongoose from 'mongoose';
import mongoose_delete from 'mongoose-delete';
import integerValidator from 'mongoose-integer';

var Schema = mongoose.Schema

/**
 *  Schema
 */
var EvolutionSchema = new Schema({
  TutorId: {
    type: String,
    required: "Tutor cannot be Blank"
  },
  StudentId: {
    type: String,
    required: "Student cannot be Blank"
  },
  PackageId: {
    type: String,
    required: "Package cannot be Blank"
  },
  PaperSetScore: [{
    setId: {
      type: String,
      
    },
    ta: {
      type: String,
    },
    cc: {
      type: String,
    },
    lr: {
      type: String,
    },
    gra: {
      type: String,
    },
    total: {
      type: String,
    },
   
  }],
  score : {
    type: String,
  },
  conclusion: {
    type: String,
 },
  
  createdAt: {
    type: Date,
    default: Date.now,
    immutable: true,
    required: "Created Date Cannot Be Blank"
  },
  lastUpdatedAt: {
    type: Date,
    default: Date.now
  }
});

EvolutionSchema.plugin(integerValidator);
EvolutionSchema.plugin(mongoose_delete, { deletedAt: true,  overrideMethods: true});
mongoose.model('Evolution', EvolutionSchema);
export default EvolutionSchema








