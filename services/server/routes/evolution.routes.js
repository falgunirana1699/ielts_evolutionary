'use strict';

/**
 * Module dependencies
 */
import  { isAllowed }from '../policies/evolution.policy';
import { list, create, read, update, remove} from '../controllers/v1/evolution.controller'

export const routes = (app)  => {
 
  app.route('/api/v1/evolutions').all(isAllowed)
    .get(list)
    .post(create);


  app.route('/api/v1/evolutions/:evolutionId').all(isAllowed)
    .get(read)
    .put(update)
    .delete(remove);

};
