/**
 * Module dependencies
 */

import {getErrorMessage} from "./errorHandler";
import _ from "lodash";
import mongoose from "mongoose";
const Cryptr = require('cryptr');

let Evolution = mongoose.model('Evolution');

const createEvolution = (data) =>{
    data = addTimestampToPayLoad(data);
    // data = encryptPassword(data);
    return new Evolution(data).save();
}

const encryptPassword = (payload) => {
    const cryptr = new Cryptr('G0@ver5Ea5');
    payload.password = cryptr.encrypt(payload.password);
    console.log(payload);
    return payload;
}

const addTimestampToPayLoad = (payload) => {
    let createdAt = new Date().toISOString();
    payload.createdAt = createdAt;
    payload.lastUpdatedAt = createdAt;
    return payload;
}

export const create = async (req, res) => {

    let data = _.cloneDeep(req.body);

    try{
        const result = await createEvolution(data);
        res.json({
          success: true,
          body: result,
        });
    }
    catch(error){
        res.status(422).send({
            success:false,
            message:getErrorMessage(error)
        });
    }
    return 


};
