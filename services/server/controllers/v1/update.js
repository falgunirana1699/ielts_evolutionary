/**
 * Module dependencies
 */
import { removeImmutableValues } from "./immutableFields";
import { getErrorMessage } from "./errorHandler";
import _ from "lodash";
import mongoose from "mongoose";
let Evolution = mongoose.model('Evolution');

const updateEvolution = (id, dataToUpdate = {}) =>{

    let params = {
        _id: id
    };
    let returnUpdatedDoc = {
        new: true
    };    
    return Evolution.findOneAndUpdate(params, dataToUpdate, returnUpdatedDoc);
    
}

const addLastUpdatedTimeStampToPayLoad = (payload) =>{

    let createdAt = new Date().toISOString();
    payload.lastUpdatedAt = createdAt;
  
    return payload;
}
  

export const update = async(req, res) => {

    const evolutionId = req.params.evolutionId;  
    if (!mongoose.Types.ObjectId.isValid(evolutionId)) {
      return res.status(400).send({
        message: 'Id is invalid'
      });
    }
  
    let data = _.cloneDeep(req.body);
    data = addLastUpdatedTimeStampToPayLoad(data);
    data = removeImmutableValues(data);
  
    try {
      let result = await updateEvolution(evolutionId, data);
      res.json({
        success: true,
        body: result,
      });
    } catch (error) {
      res.status(422).send({
        success:false,
        message:getErrorMessage(error)
      });    
    }
  
    return;
  
};
  
