/**
 * Module dependencies
 */

import {getErrorMessage} from "./errorHandler";
import _ from "lodash";
import mongoose from "mongoose";
const Cryptr = require('cryptr');


let Evolution = mongoose.model('Evolution');
const DEFAULT_PAGE_SIZE = 5;
const DEFAULT_OFFSET = 0;
const DEFAULT_PROJECTION = {
  __v    : false
};
const DEFAULT_SORT = {
  createdAt: 'desc'
}

const getParameters = (req)=>{
  let query = getQueryParameters(req);
  let [offset, pageSize] = getOffsetAndPageSize(req);
  return [query, offset, pageSize];
}

const filterAndProjectList = (query = {}, projection = {},
     offset = DEFAULT_OFFSET, pageSize = DEFAULT_PAGE_SIZE, sort = DEFAULT_SORT) => {
    let countPromise = Evolution.count(query);
    let findPromise = Evolution.find(query, projection).sort(sort).skip(offset).limit(pageSize);
    return Promise.all([countPromise, findPromise]);
  
}

const getSortOrder = (req) =>{
  try {
    let params = req.query;
    if(params.sort){
      return JSON.parse(params.sort);
    }
  } catch (error) {
    console.log(error);
    return DEFAULT_SORT;  
  }
  
}

const getOffsetAndPageSize = (req)=>{
  let params = req.query;
  return [parseInt(params.offset || DEFAULT_OFFSET), parseInt(params.pageSize || DEFAULT_PAGE_SIZE)];

}

const getQueryParameters = (req) =>{
  const cryptr = new Cryptr('G0@ver5Ea5');
  try {
    let params = req.query || {};
    let query = params.query || {};
    query = _.isString(query) ? JSON.parse(query) : query;
    // console.log(query);
    // console.log(query.query.password);
    // if (query.query.password !== undefined) {
    //   query.query.password = cryptr.encrypt(query.query.password);
    // }
    console.log(query);
    return query;

  } catch (error) {
    console.log(error);  
    throw error.message;
  }
  
}


export const list = async (req, res) => {

  let projection = DEFAULT_PROJECTION;

  try {
    let [queryParams, offset, pageSize ] = getParameters(req);
    let sort = getSortOrder(req);
    
    const result = await filterAndProjectList(queryParams, projection, offset, pageSize, sort);

    res.json({
      success: true,
      body: {
          totalCount: result[0],
          data: result[1]
      },      
    });
    return;
  } catch (error) {
    res.status(422).send({
      success:false,
      reason: error
    });
    return;
    
  }
  
};
  
