/**
 * Module dependencies
 */

import {getErrorMessage} from "./errorHandler";
import _ from "lodash";
import mongoose from "mongoose";
let Evolution = mongoose.model('Evolution');

const removeEvolution = (id) =>{
    let params = {
      _id: id
    };
    return Evolution.delete(params)
}

export const remove = async(req, res) =>{

    const evolutionId = req.params.evolutionId;  
    if (!mongoose.Types.ObjectId.isValid(evolutionId)) {
      return res.status(400).send({
        message: 'Id is invalid'
      });
    }
  
    try {
      let result = await removeEvolution(evolutionId);
      res.json({
        success: true,
        body: result,
      });
    } catch (error) {
      res.status(422).send({
        success:false,
        message:getErrorMessage(error)
      });    
    }
  
    return; 
  
  };
  
  
