
import Server from "../base-server/index"
import { EvolutionSchema } from "./server/models/evolution.model"
import { routes } from "./server/routes/evolution.routes"
import { config } from "./config/evolution.config"

const configureAndStartBaseServer = async() =>{
    try {

        let baseServer = new Server(config);
        let [db, app] = await baseServer.start(routes); 
    } catch (error) {
        console.log(error);    
    }
}
configureAndStartBaseServer()
export default configureAndStartBaseServer