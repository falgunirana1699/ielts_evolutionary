
import express from "../lib/express";
import _ from "lodash";
import chalk from "chalk";
import config from "../config/config";
import {connect, disconnect} from "../lib/mongoose";

class Server{

  constructor(configuration = {}){
    this.configuration = _.merge(config, configuration);
    this.init();
  }

  init() {
    this.app = express.init();
  }

  async start(router = _.noop) {
    router(this.app);
    return Promise.all([
      this._connectToDB(),
      this._listen()
    ]);
  }

  _connectToDB() {
      return connect(this.configuration);
  }

  _listen(port = this.configuration.port, host = this.configuration.host) {
    let listenPromise = new Promise((resolve, reject)=>{

      this.app.listen(port, host, ()=>{

        var server = (process.env.NODE_ENV === 'secure' ? 'https://' : 'http://') + host + ':' + port;
        console.log('--');
        console.log();
        console.log(chalk.green('Environment:     ' + process.env.NODE_ENV));
        console.log(chalk.green('Server:          ' + server));
        console.log(chalk.green('Database:        ' + this.configuration.db.uri));
        console.log('--');
        resolve(this.app);       
      });

    });
    return listenPromise;
  }
    
}



export default Server
