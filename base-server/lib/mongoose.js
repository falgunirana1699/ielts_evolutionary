'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
  config = require('../config/config'),
  chalk = require('chalk'),
  path = require('path'),
  mongoose = require('mongoose');



// Initialize Mongoose
export const connect =  (configuration) => {
  mongoose.Promise = config.db.promise;
  configuration = configuration || {};
  var options = _.merge(config.db.options || configuration, { useMongoClient: true });
  mongoose
    .connect(config.db.uri, options)
    .then(function (connection) {
      // Enabling mongoose debug mode if required
      mongoose.set('debug', config.db.debug);

      return connection.db;
    })
    .catch(function (err) {
      console.error(chalk.red('Could not connect to MongoDB!'));
      console.log(err);
      throw err;
    });

};

export const disconnect =  (cb) => {
  mongoose.connection.db
    .close(function (err) {
      console.info(chalk.yellow('Disconnected from MongoDB.'));
      return cb(err);
    });
};
